package com.example.android.sunshine;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private final static String DATA = "data";
    private String weather;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weather = getArguments().getString(DATA);
    }


    public static DetailActivityFragment newInstance(String weather) {
        DetailActivityFragment ofTheJedi = new DetailActivityFragment();
        Bundle bundle = new Bundle();
        bundle.putString(DATA, weather);
        ofTheJedi.setArguments(bundle);
        return ofTheJedi;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        ((TextView) v.findViewById(R.id.weather_text)).setText(weather);
        return v;
    }
}
