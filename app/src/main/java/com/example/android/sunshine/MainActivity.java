package com.example.android.sunshine;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "ON CREATE");
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i(TAG, "ON START");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(TAG, "ON RESUME");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.i(TAG, "ON PAUSE");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.i(TAG, "ON STOP");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.i(TAG, "ON DESTROY");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_view) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String zipcode = sharedPreferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default));
            zipcode = "geo:0,0?q=" + zipcode;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(zipcode));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
